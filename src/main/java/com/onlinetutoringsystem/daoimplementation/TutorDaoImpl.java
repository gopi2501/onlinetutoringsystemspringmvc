package com.onlinetutoringsystem.daoimplementation;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.onlinetutoringsystem.entity.Tutor;
import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.interfaces.TutorInterface;
import com.onlinetutoringsystem.rowmapper.UserRowMapper;

public class TutorDaoImpl implements TutorInterface{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	

	@Override
	public Tutor findTutorById(int userid) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select * from user where userid =" + userid;
		Tutor tutor;
		try {
			tutor = (Tutor) jdbcTemplate.queryForObject(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Tutor Found");
			return null;
		}
		return tutor;
	}

	@Override
	public Tutor findTutorByName(String username) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select * from user where username =" + username;
		Tutor tutor;
		try {
			tutor = (Tutor) jdbcTemplate.queryForObject(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Tutor Found");
			return null;
		}
		return tutor;
	}

	@Override
	public Tutor findUserIdFromTutorId(int tutorid) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select * from tutor where tutorid =" + tutorid;
		Tutor tutor;
		try {
			tutor = (Tutor) jdbcTemplate.queryForObject(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Tutor Found");
			return null;
		}
		return tutor;
	}
	
	public int returnUserIdFromTutorId(int tutorid) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select userid from tutor where tutorid = ?";
		int userid;
		Tutor tutor;
		try {
			userid = Integer.parseInt((String)jdbcTemplate.queryForObject(sql, new Object[] { tutorid}, String.class));
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(TutorDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Tutor Found");
			return 0;
		}
		return userid;
		
	}

	public String findTutorIdByUserIdFromTutorTable(int userid) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select tutorid from tutor where userid = ?" ;
		String idInString=null;
		try {
			
			idInString= jdbcTemplate.queryForObject(sql, new Object[] { userid }, String.class);
		
		
		}catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(TutorDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Student Found");
			idInString= null;
		}
		return idInString;
	}

}
