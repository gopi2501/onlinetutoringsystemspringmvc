package com.onlinetutoringsystem.daoimplementation;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.interfaces.StudentInterface;
import com.onlinetutoringsystem.rowmapper.UserRowMapper;

public class StudentDaoImpl implements StudentInterface {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public User findStudentById(int id) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select * from user where userid =" + id;
		User user;
		try {
			user = (User) jdbcTemplate.queryForObject(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Student Found");
			return null;
		}
		return user;
		
	
	}
	@Override
	public User findStudentByName(String username) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select * from user where username =" +username ;
		User user;
		try {
			user = (User) jdbcTemplate.queryForObject(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Student Found");
			return null;
		}
		return user;
	}
	
	
	
	
//	@Override
//	public int findStudentIdByUserIdFromStudentTable(int userid) {
//		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//		dataSource = (DataSource) context.getBean("dataSource");
//		jdbcTemplate = new JdbcTemplate(dataSource);
//		String sql = "select * from student where userid =" +userid ;
//		int id;
//		try {
//		String idInString= jdbcTemplate.queryForObject(sql, new Object[] { userid }, String.class);
//		id= Integer.parseInt(idInString);
//		
//		}catch (EmptyResultDataAccessException e) {
//			Logger logger = Logger.getLogger(LoginDaoImpl.class);
//			BasicConfigurator.configure();
//			logger.info("No Student Found");
//			id= 0;
//		}
//		return id;
//	}
	
	@Override
	public String findStudentIdByUserIdFromStudentTable(int userid) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select studentid from student where userid = ?" ;
		String idInString=null;
		try {
			
			idInString= jdbcTemplate.queryForObject(sql, new Object[] { userid }, String.class);
		
		
		}catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Student Found");
			idInString= null;
		}
		return idInString;
	}
	
}
