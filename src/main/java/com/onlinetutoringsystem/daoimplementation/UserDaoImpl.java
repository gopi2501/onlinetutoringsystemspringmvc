package com.onlinetutoringsystem.daoimplementation;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.onlinetutoringsystem.interfaces.UserInterface;

public class UserDaoImpl implements UserInterface {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	
	
	@Override
	public String userNameFromUserId(int userid) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select firstName from user where userid = ?" ;
		String idInString=null;
		try {
			
			idInString= jdbcTemplate.queryForObject(sql, new Object[] { userid }, String.class);
		
		
		}catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Student Found");
			idInString= null;
		}
		return idInString;
		
	
	}
	

}
