package com.onlinetutoringsystem.daoimplementation;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import java.sql.ResultSet;

import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.interfaces.LoginInterface;
import com.onlinetutoringsystem.rowmapper.UserRowMapper;

public class LoginDaoImpl implements LoginInterface {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	@Override
	public User findUserByUsernameAndPassword(String username, String password) {
		// ApplicationContext context= new
		// ClassPathXmlApplicationContext("applicationContext.xml");
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select * from user where username ='" + username + "' AND password='" + password + "'";
		User user;
		try {
			user = (User) jdbcTemplate.queryForObject(sql, new UserRowMapper());
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Such User is being found");
			return null;
		}
		return user;
	}

}
