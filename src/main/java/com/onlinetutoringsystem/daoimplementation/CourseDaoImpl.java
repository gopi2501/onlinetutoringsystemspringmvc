package com.onlinetutoringsystem.daoimplementation;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.interfaces.CourseInterface;
import com.onlinetutoringsystem.rowmapper.UserRowMapper;

public class CourseDaoImpl implements CourseInterface {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	@Override
	public String getCourseNameUsingId(int courseid) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "select coursename from course where courseid = ?";
		String coursename = null;
		try {

			coursename = (String) jdbcTemplate.queryForObject(sql, new Object[] { courseid }, String.class);
		} catch (EmptyResultDataAccessException e) {
			Logger logger = Logger.getLogger(LoginDaoImpl.class);
			BasicConfigurator.configure();
			logger.info("No Course Found");
			return null;
		}
		return coursename;

	}

	@Override
	public List<Map<String, Object>> getCourseStatusUsingCourseIdAndRole(int stuOrTutorId, String role) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dataSource = (DataSource) context.getBean("dataSource");
		jdbcTemplate = new JdbcTemplate(dataSource);
		if (role.equals("student")) {
			String sql = "select courseid, courseenddate, tutorid from course_availability where studentid='"
					+ stuOrTutorId + "';";
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
			return rows;
		} else if (role.equals("tutor")) {
			String sql = "select courseid, courseenddate, studentid from course_availability where tutorid='"
					+ stuOrTutorId + "';";
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
			return rows;
		} else
			return null;

	}

}
