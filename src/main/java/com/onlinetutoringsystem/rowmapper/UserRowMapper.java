package com.onlinetutoringsystem.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.onlinetutoringsystem.entity.User;

public class UserRowMapper implements RowMapper {

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		User user= new User();
		try {
			
			user.setUsername(rs.getString("username"));
			user.setFirstName(rs.getString("firstName"));
			user.setLastName(rs.getString("lastName"));
			user.setRole(rs.getString("role"));
			user.setUserid(rs.getInt("userid"));
		} catch (SQLException ex) {
			user.setRole("No such User");
		}
		
		
		return user;
	}

}
