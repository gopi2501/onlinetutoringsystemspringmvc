package com.onlinetutoringsystem.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.services.CourseService;
import com.onlinetutoringsystem.services.StudentService;
import com.onlinetutoringsystem.services.TutorService;

@Controller
public class CentralController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {

		return new ModelAndView("index");
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	// @RequestBody
	public ModelAndView login(@RequestParam("username") String username, @RequestParam("password") String password) {
		System.out.println(username);
		System.out.println(password);
		ModelAndView mymodel = null;
		CourseService courseService; StudentService studentservice; 
		TutorService tutorservice;
		LoginController logincontroller = new LoginController();
		User user = logincontroller.validateUser(username, password);
		if (user == null) {
			mymodel = new ModelAndView("loginfailed");
			mymodel.addObject("loginstatus", "No Such User :(");

		} else {
			if (user.getRole().equals("student")) {
				
				mymodel = new ModelAndView("StudentHome");
				System.out.println(user.getRole());
				courseService= new CourseService(); 
				studentservice= new StudentService();
				mymodel.addObject("courseStatusMap", courseService.getCourseStatus(studentservice.findStudentIdByUserIdFromStudentTable(user.getUserid()), user.getRole()));
				mymodel.addObject("loginstatus", "Login Successful !!!!");
				mymodel.addObject("student", user);
				return mymodel;
			} else if (user.getRole().equals("tutor")) {
				mymodel = new ModelAndView("TutorHome");
				System.out.println(user.getRole());
				courseService= new CourseService();
				tutorservice= new TutorService();
				mymodel.addObject("courseStatusMap", courseService.getCourseStatus(tutorservice.findTutorIdByUserIdFromTutorTable(user.getUserid()), user.getRole()));
//				mymodel.addObject("courseStatusMap", courseService.getCourseStatus(TUTORID, role))
				
				mymodel.addObject("loginstatus", "Login Successful !!!!");
				mymodel.addObject("tutor", user);
				return mymodel;
			}

		}

		return mymodel;
	}

}
