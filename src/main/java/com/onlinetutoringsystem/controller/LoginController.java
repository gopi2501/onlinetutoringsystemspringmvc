package com.onlinetutoringsystem.controller;

import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.services.LoginService;

public class LoginController {

	public User validateUser(String username, String password) {
		User user= new User();
		user.setUsername(username);
		user.setPassword(password);
		LoginService loginservice= new LoginService();
		User thisuser= loginservice.validateUser(user);
		return thisuser;
	}
	
	
}
