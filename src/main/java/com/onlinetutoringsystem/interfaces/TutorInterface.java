package com.onlinetutoringsystem.interfaces;

import com.onlinetutoringsystem.entity.Tutor;

public interface TutorInterface {
	
	public Tutor findTutorById(int id);
	public Tutor findTutorByName(String username);
	public Tutor findUserIdFromTutorId(int tutorid);

}
