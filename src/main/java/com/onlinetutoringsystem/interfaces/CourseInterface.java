package com.onlinetutoringsystem.interfaces;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public interface CourseInterface {

	public String getCourseNameUsingId(int courseid);
	public List<Map<String, Object>> getCourseStatusUsingCourseIdAndRole(int stuOrTutorId, String role);
	
}
