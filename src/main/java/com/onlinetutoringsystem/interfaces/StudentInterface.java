package com.onlinetutoringsystem.interfaces;

import com.onlinetutoringsystem.entity.User;

public interface StudentInterface {

	public User findStudentById(int id);
	public User findStudentByName(String username);
	public String findStudentIdByUserIdFromStudentTable(int userid);
	
}
