package com.onlinetutoringsystem.interfaces;

import com.onlinetutoringsystem.entity.User;

public interface LoginInterface {
	
//	public User findUser(String username, String password);

	public User findUserByUsernameAndPassword(String username, String password);

}
