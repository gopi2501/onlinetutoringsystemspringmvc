package com.onlinetutoringsystem.entity;

public class CourseStatusDetails {
	
	private String courseName, tutorName, status, studentName;

	public CourseStatusDetails() {
		super();
	}

	public CourseStatusDetails(String courseName, String tutorName, String status, String studentName) {
		super();
		this.studentName=studentName;
		this.courseName = courseName;
		this.tutorName = tutorName;
		this.status = status;
	}
	
	

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTutorName() {
		return tutorName;
	}

	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
