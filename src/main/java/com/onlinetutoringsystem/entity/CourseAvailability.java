package com.onlinetutoringsystem.entity;

public class CourseAvailability {

	private int courseid, tutorid, studentid, price;
	private String coursename, courseslot, coursestartdate, courseenddate;
	public CourseAvailability() {
		super();
	}
	public CourseAvailability(int courseid, int tutorid, int studentid, int price, String coursename, String courseslot,
			String coursestartdate, String courseenddate) {
		super();
		this.courseid = courseid;
		this.tutorid = tutorid;
		this.studentid = studentid;
		this.price = price;
		this.coursename = coursename;
		this.courseslot = courseslot;
		this.coursestartdate = coursestartdate;
		this.courseenddate = courseenddate;
	}
	public int getCourseid() {
		return courseid;
	}
	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}
	public int getTutorid() {
		return tutorid;
	}
	public void setTutorid(int tutorid) {
		this.tutorid = tutorid;
	}
	public int getStudentid() {
		return studentid;
	}
	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public String getCourseslot() {
		return courseslot;
	}
	public void setCourseslot(String courseslot) {
		this.courseslot = courseslot;
	}
	public String getCoursestartdate() {
		return coursestartdate;
	}
	public void setCoursestartdate(String coursestartdate) {
		this.coursestartdate = coursestartdate;
	}
	public String getCourseenddate() {
		return courseenddate;
	}
	public void setCourseenddate(String courseenddate) {
		this.courseenddate = courseenddate;
	}
	
	
	
}
