package com.onlinetutoringsystem.entity;

import java.sql.Date;

public class Tutor extends User {

	private Date dob;
	private String firstName, lastName;
	private int rating;
	
	public Tutor() {
		super();
	}
	


	public Tutor(Date dob, String firstName, String lastName, int rating) {
		super();
		this.dob = dob;
		this.firstName = firstName;
		this.lastName = lastName;
		this.rating = rating;
	}



	public Date getDob() {
		return dob;
	}



	public void setDob(Date dob) {
		this.dob = dob;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	
	
}
