package com.onlinetutoringsystem.entity;

import java.sql.Date;

public class User {
	private int userid;
	private String username, role, password, email, gender;
	private Date dob;
	private String firstName, lastName;
	
	public User() {
		super();
	}

	public User(int userid, String username, String role, String password, String email, String gender, Date dob, String firstName,
			String lastName) {
		super();
		this.userid=userid;
		this.username = username;
		this.role = role;
		this.password = password;
		this.email = email;
		this.gender = gender;
		this.dob = dob;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	
		
	

}
