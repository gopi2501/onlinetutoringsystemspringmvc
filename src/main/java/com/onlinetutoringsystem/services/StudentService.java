package com.onlinetutoringsystem.services;

import com.onlinetutoringsystem.daoimplementation.StudentDaoImpl;
import com.onlinetutoringsystem.entity.Tutor;
import com.onlinetutoringsystem.entity.User;
import com.onlinetutoringsystem.interfaces.StudentInterface;

public class StudentService {

	public int findStudentIdByUserIdFromStudentTable(int userid) {
		
		StudentDaoImpl studentdao= new StudentDaoImpl();
		String studentIdinString=studentdao.findStudentIdByUserIdFromStudentTable(userid);
		if(studentIdinString==null) {
			return 0;
		} else {
			return Integer.parseInt(studentIdinString);
		}
	}
	
//	public static void main(String[] args) {
//		StudentService studentservice= new StudentService();
//		System.out.println(studentservice.findStudentIdByUserIdFromStudentTable(111));
//	}
	


}
