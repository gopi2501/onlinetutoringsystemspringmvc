package com.onlinetutoringsystem.services;

import com.onlinetutoringsystem.daoimplementation.LoginDaoImpl;
import com.onlinetutoringsystem.entity.User;

public class LoginService {
	
	public User validateUser(User user) {
		LoginDaoImpl logindao= new LoginDaoImpl();
		User loggedUser= logindao.findUserByUsernameAndPassword(user.getUsername(), user.getPassword());
		return loggedUser;
	}

}
