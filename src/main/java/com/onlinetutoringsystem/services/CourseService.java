package com.onlinetutoringsystem.services;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.onlinetutoringsystem.daoimplementation.CourseDaoImpl;
import com.onlinetutoringsystem.daoimplementation.StudentDaoImpl;
import com.onlinetutoringsystem.daoimplementation.TutorDaoImpl;
import com.onlinetutoringsystem.daoimplementation.UserDaoImpl;
import com.onlinetutoringsystem.entity.CourseStatusDetails;

public class CourseService {
	
	CourseDaoImpl coursedao;
	TutorDaoImpl tutordao;
	StudentDaoImpl studentdao;
	UserDaoImpl userdao;

	public static String getStatus(Date courseEndDate) {
		Calendar currenttime = Calendar.getInstance();
		Date sqldate = new Date((currenttime.getTime()).getTime());
		int datedifference = sqldate.compareTo(courseEndDate);
		if (datedifference >= 0) {
			return "@Completed";
		} else {
			Format formatter = new SimpleDateFormat("yyyy-MM-dd");
			String formatedDate = formatter.format(courseEndDate);
			return "@targeted" + formatedDate.toString();
		}
	}
	
	public List<CourseStatusDetails> getCourseStatus(int stuOrTutorId, String role) {
		Map<String, String> coursestatus = new HashMap<String, String>();
		coursedao = new CourseDaoImpl();
		tutordao= new TutorDaoImpl();
		studentdao= new StudentDaoImpl();
		userdao= new UserDaoImpl();
		
		ArrayList<CourseStatusDetails> courseStatusList= new ArrayList<CourseStatusDetails>();
		
		List<Map<String, Object>> courserows = coursedao.getCourseStatusUsingCourseIdAndRole(stuOrTutorId, role);
		
		for (Map row : courserows) {
			CourseStatusDetails courseStatus= new CourseStatusDetails();
			courseStatus.setCourseName((String) coursedao.getCourseNameUsingId((Integer) row.get("courseid")));
			courseStatus.setStatus((String)CourseService.getStatus((java.sql.Date) row.get("courseenddate")));
			if(role.equals("student")) {
				int tutorid= (Integer) row.get("tutorid");
				courseStatus.setTutorName(userdao.userNameFromUserId(tutordao.returnUserIdFromTutorId(tutorid)));	
			} else if(role.equals("tutor")) {
				int studentid= (Integer) row.get("studentid");
				courseStatus.setStudentName(userdao.userNameFromUserId(tutordao.returnUserIdFromTutorId(studentid)));
			}
			
			courseStatusList.add(courseStatus);
		}
		return courseStatusList;
	}

//	public Map<String, String> getCourseStatus(int stuOrTutorId, String role) {
//		Map<String, String> coursestatus = new HashMap<String, String>();
//		coursedao = new CourseDaoImpl();
//		tutordao= new TutorDaoImpl();
//		studentdao= new StudentDaoImpl();
//		
//		
//		List<Map<String, Object>> courserows = coursedao.getCourseStatusUsingCourseIdAndRole(stuOrTutorId, role);
//		
//		for (Map row : courserows) {
//			
//			coursestatus.put((String) coursedao.getCourseNameUsingId((Integer) row.get("courseid")),
//					CourseService.getStatus((java.sql.Date) row.get("courseenddate")));
//		}
//		return coursestatus;
//	}

//	 public static void main(String[] args) {
//	 CourseDaoImpl coursedao= new CourseDaoImpl();
//	 CourseService courseservice= new CourseService();
////	 System.out.println(coursedao.getCourseStatusUsingCourseIdAndRole(221,"student"));
//	 System.out.println(courseservice.getCourseStatus(221, "student"));
//	 }
	

}
