package com.onlinetutoringsystem.services;

import com.onlinetutoringsystem.daoimplementation.TutorDaoImpl;

public class TutorService {

	public int findTutorIdByUserIdFromTutorTable(int userid) {

		TutorDaoImpl tutordao = new TutorDaoImpl();
		String studentIdinString = tutordao.findTutorIdByUserIdFromTutorTable(userid);
		if (studentIdinString == null) {
			return 0;
		} else {
			return Integer.parseInt(studentIdinString);
		}
	}

}
